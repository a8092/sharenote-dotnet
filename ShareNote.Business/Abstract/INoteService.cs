﻿using ServiceRepository.Repository;
using ShareNote.DAL.Dto.Note;

namespace ShareNote.Business.Abstract
{
    public interface INoteService : IBaseService<NoteDto>
    {
    }
}
