﻿using Microsoft.EntityFrameworkCore;
using ServiceRepository.Repository;
using ShareNote.Business.Abstract;
using ShareNote.DAL.Dto.Note;
using ShareNote.DAL.Entity;

namespace ShareNote.Business.Concrete
{
    public class NoteService : BaseService<Note, NoteDto>, INoteService
    {
        public NoteService(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
