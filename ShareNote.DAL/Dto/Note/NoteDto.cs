﻿using AppCore.Entity;

namespace ShareNote.DAL.Dto.Note
{
    public class NoteDto : IBaseDto
    {
        public int Id { get; set; }
    }
}
